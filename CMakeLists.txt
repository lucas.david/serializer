cmake_minimum_required(VERSION 3.13)
project(serializer)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-Wpedantic -Wall -Wextra -Wconversion")

set(SOURCE_FILES serializer/main.c++ serializer/format.c++)

add_subdirectory(external/rapidjson)

include_directories(serializer/include external/rapidjson/include)

add_executable(serializer ${SOURCE_FILES})
