
#include <serializer/common.h++>

#include <iostream>
#include <sstream>

int main() {
  /* Serialization (to_json) */
  std::cout << "Serialization (to_json):\n";

  /* * Primitive Types */
  std::cout << to_json(nullptr) << '\n';
  std::cout << to_json(false) << '\n';
  std::cout << to_json(true) << '\n';
  std::cout << to_json(42) << '\n';
  std::cout << to_json(3.141592f) << '\n';
  std::cout << to_json(3.141592) << '\n';
  std::cout << to_json("any string") << '\n';

  std::cout << '\n';

  /* * Linear containers: arrays, vector, sets, etc... */
  std::cout << to_json({0, 1, 2}) << '\n';
  std::cout << to_json(std::array<int, 3>({0, 1, 2})) << '\n';
  std::cout << to_json(std::set<int>({0, 1, 2})) << '\n';
  std::cout << to_json(std::vector<int>({0, 1, 2})) << '\n';
  std::cout << to_json(std::vector<std::vector<int>>({{0, 1}, {2, 3}, {4, 5}})) << '\n';

  std::cout << '\n';

  /* * Associative containers: map, unordered map, etc... also with key as integer (wrapper in string) */
  std::cout << to_json({std::make_pair<int, std::string>(0, "zero"), {1, "one"}, {2, "two"}}) << '\n';
  std::cout << to_json(std::map<std::string, std::string>({{"zero", "zero"}, {"one", "one"}, {"two", "two"}})) << '\n';
  std::cout << to_json(std::map<int, std::string>({{0, "zero"}, {1, "one"}, {2, "two"}})) << '\n';
  std::cout << to_json(std::unordered_map<std::string, std::string>({{"zero", "zero"}, {"one", "one"}, {"two", "two"}})) << '\n';
  std::cout << to_json(std::unordered_map<int, std::string>({{0, "zero"}, {1, "one"}, {2, "two"}})) << '\n';

  std::cout << '\n';

  /* Deserialization (from_json) */
  std::cout << "Deserialization(from_json):\n";
  /* * Primitive Types */
  std::cout << from_json<std::nullptr_t>("null") << '\n';
  std::cout << from_json<bool>("false") << '\n';
  std::cout << from_json<bool>("true") << '\n';
  std::cout << from_json<int>("42") << '\n';
  std::cout << from_json<float>("3.141592") << '\n';
  std::cout << from_json<double>("3.141592") << '\n';
  std::cout << from_json<std::string>("\"any string\"") << '\n';

  std::cout << '\n';

  /* * Linear containers: arrays, vector, sets, etc... */
  std::cout << from_json<std::array<int, 3>>("[0,1,2]") << '\n';
  std::cout << from_json<std::set<int>>("[0,1,2]") << '\n';
  std::cout << from_json<std::vector<int>>("[0,1,2]") << '\n';
  std::cout << from_json<std::vector<std::vector<int>>>("[[0,1],[2,3],[4,5]]") << '\n';

  std::cout << '\n';

  /* * Associative containers: maps, unordered maps, etc... also with key as integer (wrapper in string) */
  std::cout << from_json<std::map<std::string, std::string>>("{\"zero\":\"zero\",\"one\":\"one\",\"two\":\"two\"}") << '\n';
  std::cout << from_json<std::map<int, std::string>>("{\"0\":\"zero\",\"1\":\"one\",\"2\":\"two\"}") << '\n';

  std::cout << '\n';

  /* Serialization (operator<<) */
  std::cout << "Serialization (operator<<):\n";

  /* * Linear containers: arrays, sets, vectors, etc... */
  std::cout << std::array<int, 3>({0, 1, 2}) << '\n';
  std::cout << std::set<int>({0, 1, 2}) << '\n';
  std::cout << std::vector<int>({0, 1, 2}) << '\n';
  std::cout << std::vector<std::vector<int>>({{0, 1}, {2, 3}, {4, 5}}) << '\n';

  std::cout << '\n';

  /* * Associative containers: maps, unordered maps, etc... also with key as integer (wrapper in string) */
  std::cout << std::map<std::string, std::string>({{"zero", "zero"}, {"one", "one"}, {"two", "two"}}) << '\n';
  std::cout << std::map<int, std::string>({{0, "zero"}, {1, "one"}, {2, "two"}}) << '\n';
  std::cout << std::unordered_map<std::string, std::string>({{"zero", "zero"}, {"one", "one"}, {"two", "two"}}) << '\n';
  std::cout << std::unordered_map<int, std::string>({{0, "zero"}, {1, "one"}, {2, "two"}}) << '\n';

  std::cout << '\n';

  /** Deserialization (operator>>) **/
  std::cout << "Deserialization(operator>>):\n";

  /* * Linear containers: arrays, sets, vector etc... */
  std::array<int, 3> serialized_array_of_integers;
  std::stringstream("[0,1,2]")
    >> make_deserializer_wrapper(serialized_array_of_integers);
  std::cout << serialized_array_of_integers << '\n';

  std::set<int> serialized_set_of_integers;
  std::stringstream("[0,1,2]")
    >> make_deserializer_wrapper(serialized_set_of_integers);
  std::cout << serialized_set_of_integers << '\n';

  std::vector<int> serialized_vector_of_integers;
  std::stringstream("[0,1,2]")
    >> make_deserializer_wrapper(serialized_vector_of_integers);
  std::cout << serialized_vector_of_integers << '\n';

  std::vector<std::vector<int>> serialized_vector_of_vectors_of_integers;
  std::stringstream("[[0,1],[2,3],[4,5]]")
    >> make_deserializer_wrapper(serialized_vector_of_vectors_of_integers);
  std::cout << serialized_vector_of_vectors_of_integers << '\n';

  std::cout << '\n';

  /* * Associative containers: maps, unordered maps, etc... also with key as integer (wrapper in string) */
  std::map<std::string, std::string> serialized_map_with_string_keys;
  std::stringstream("{\"zero\":\"zero\",\"one\":\"one\",\"two\":\"two\"}")
    >> make_deserializer_wrapper(serialized_map_with_string_keys);
  std::cout << serialized_map_with_string_keys << '\n';

  std::map<int, std::string> serialized_map_with_integer_keys;
  std::stringstream("{\"0\":\"zero\",\"1\":\"one\",\"2\":\"two\"}")
    >> make_deserializer_wrapper(serialized_map_with_integer_keys);
  std::cout << serialized_map_with_integer_keys << '\n';

  std::unordered_map<std::string, std::string> serialized_unordered_map_with_string_keys;
  std::stringstream("{\"zero\":\"zero\",\"one\":\"one\",\"two\":\"two\"}")
    >> make_deserializer_wrapper(serialized_unordered_map_with_string_keys);
  std::cout << serialized_unordered_map_with_string_keys << '\n';

  std::unordered_map<int, std::string> serialized_unordered_map_with_integer_keys;
  std::stringstream("{\"0\":\"zero\",\"1\":\"one\",\"2\":\"two\"}")
    >> make_deserializer_wrapper(serialized_unordered_map_with_integer_keys);
  std::cout << serialized_unordered_map_with_integer_keys << '\n';

  std::cout << '\n';

  /** Error handling (mostly for debug via exception; cost at failure only) */
  auto const catch_and_continue = [] (auto function) {
    try {
      function();
    } catch (std::runtime_error const& e) {
      std::cout << e.what() << '\n';
    }
  };

  std::cout << "Error handling:\n";

  catch_and_continue([] {
    std::cout << from_json<std::nullptr_t>("3.14") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<bool>("null") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<bool>("null") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<int>("null") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<float>("null") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<double>("null") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<std::string>("null") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<std::array<int, 2>>("[0, 1, 2]") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<std::array<int, 3>>("[0, 1, 2") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<std::map<int, int>>("{\"0\": 0, \"1\": 1, \"2\": 2") << '\n';
  });

  catch_and_continue([] {
    std::cout << from_json<int>("0, 1") << '\n';
  });

  return 0;
}
