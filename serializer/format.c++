#include <serializer/deserialize/deserialization_error.h++>

#include <cassert>

char const* format(rapidjson::ParseErrorCode parse_error_code) {
  switch (parse_error_code) {
  case rapidjson::ParseErrorCode::kParseErrorNone:
    return "no error";
  case rapidjson::ParseErrorCode::kParseErrorDocumentEmpty:
    return "document is empty";
  case rapidjson::ParseErrorCode::kParseErrorDocumentRootNotSingular:
    return "document root must not follow by other values";
  case rapidjson::ParseErrorCode::kParseErrorValueInvalid:
    return "invalid value";
  case rapidjson::ParseErrorCode::kParseErrorObjectMissName:
    return "missing a name for an object member";
  case rapidjson::ParseErrorCode::kParseErrorObjectMissColon:
    return "missing a colon after a name of object member";
  case rapidjson::ParseErrorCode::kParseErrorObjectMissCommaOrCurlyBracket:
    return "missing a comma or '}' after an object member";
  case rapidjson::ParseErrorCode::kParseErrorArrayMissCommaOrSquareBracket:
    return "missing a comma or ']' after an array element";
  case rapidjson::ParseErrorCode::kParseErrorStringUnicodeEscapeInvalidHex:
    return "incorrect hexadecimal digit after '\\u' escape in string";
  case rapidjson::ParseErrorCode::kParseErrorStringUnicodeSurrogateInvalid:
    return "surrogate pair in string is invalid";
  case rapidjson::ParseErrorCode::kParseErrorStringEscapeInvalid:
    return "invalid espace character in string";
  case rapidjson::ParseErrorCode::kParseErrorStringMissQuotationMark:
    return "missing a closing '\"' in string";
  case rapidjson::ParseErrorCode::kParseErrorStringInvalidEncoding:
    return "invalid encoding in string";
  case rapidjson::ParseErrorCode::kParseErrorNumberTooBig:
    return "number too big to be stored in double";
  case rapidjson::ParseErrorCode::kParseErrorNumberMissFraction:
    return "miss fraction part in number";
  case rapidjson::ParseErrorCode::kParseErrorNumberMissExponent:
    return "miss exponent part in number";
  case rapidjson::ParseErrorCode::kParseErrorTermination:
    return "parsing was terminated";
  case rapidjson::ParseErrorCode::kParseErrorUnspecificSyntaxError:
    return "unspecific syntax error";
  default:
    assert("unknown rapidjson::ParseErrorCode" and false);
    throw std::runtime_error("unknown rapidjson::ParseErrorCode.");
  }
}

char const* format(rapidjson::Type type) {
  switch (type) {
  case rapidjson::Type::kNullType:
    return "json::null";
  case rapidjson::Type::kFalseType:
  case rapidjson::Type::kTrueType:
    return "json::bool";
  case rapidjson::Type::kArrayType:
    return "json::array";
  case rapidjson::Type::kObjectType:
    return "json::object";
  case rapidjson::Type::kStringType:
    return "json::string";
  case rapidjson::Type::kNumberType:
    return "json::number";
  default:
    assert("unknown rapidjson::Type" and false);
    throw std::runtime_error("unknown rapidjson::Type.");
  }
}
