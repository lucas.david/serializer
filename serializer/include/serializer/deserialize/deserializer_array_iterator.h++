
#ifndef SERIALIZER_DESERIALIZE_DESERIALIZER_ARRAY_ITERATOR
#define SERIALIZER_DESERIALIZE_DESERIALIZER_ARRAY_ITERATOR

#include <rapidjson/document.h>

#include <iterator>
#include <type_traits>

template<typename T>
struct deserializer_array_iterator : public std::iterator<std::random_access_iterator_tag, T> {
public:
  static_assert(std::is_default_constructible<T>::value, "T is not default constructible.");

  using iterator_category = typename std::iterator<std::random_access_iterator_tag, T>::iterator_category;
  using value_type        = typename std::iterator<std::random_access_iterator_tag, T>::value_type;
  using difference_type   = typename std::iterator<std::random_access_iterator_tag, T>::difference_type;
  using pointer           = typename std::iterator<std::random_access_iterator_tag, T>::pointer;
  using reference         = typename std::iterator<std::random_access_iterator_tag, T>::reference;

  deserializer_array_iterator(rapidjson::Value::ConstArray::ValueIterator iterator) noexcept : iterator_(iterator) {}

  deserializer_array_iterator& operator++() noexcept {
    ++iterator_;
    return *this;
  }

  deserializer_array_iterator operator++(int) noexcept {
    deserializer_array_iterator result = *this;
    iterator_++;
    return result;
  }

  deserializer_array_iterator& operator--() noexcept {
    --iterator_;
    return *this;
  }

  deserializer_array_iterator operator--(int) noexcept {
    deserializer_array_iterator result = *this;
    iterator_--;
    return result;
  }

  deserializer_array_iterator& operator+=(difference_type n) noexcept {
    iterator_ += n;
    return *this;
  }

  deserializer_array_iterator& operator-=(difference_type n) noexcept {
    iterator_ -= n;
    return *this;
  }

  deserializer_array_iterator operator+(difference_type n) const noexcept {
    deserializer_array_iterator result = *this;
    result.iterator_ = iterator_ + n;
    return result;
  }

  deserializer_array_iterator operator-(difference_type n) const noexcept {
    deserializer_array_iterator result = *this;
    result.iterator_ = iterator_ - n;
    return result;
  }

  difference_type operator-(deserializer_array_iterator rhs) const noexcept { return iterator_ - rhs.iterator_; }

  bool operator==(deserializer_array_iterator rhs) const noexcept { return iterator_ == rhs.iterator_; }
  bool operator!=(deserializer_array_iterator rhs) const noexcept { return iterator_ != rhs.iterator_; }
  bool operator<=(deserializer_array_iterator rhs) const noexcept { return iterator_ <= rhs.iterator_; }
  bool operator>=(deserializer_array_iterator rhs) const noexcept { return iterator_ >= rhs.iterator_; }
  bool operator<(deserializer_array_iterator rhs) const noexcept { return iterator_ < rhs.iterator_; }
  bool operator>(deserializer_array_iterator rhs) const noexcept { return iterator_ > rhs.iterator_; }

  value_type operator*() const {
    T result;
    *iterator_ >> result;
    return result;
  }

private:
  rapidjson::Value::ConstArray::ValueIterator iterator_;
};

#endif
