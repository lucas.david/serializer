#ifndef SERIALIZER_DESERIALIZE_COMMON
#define SERIALIZER_DESERIALIZE_COMMON

#include <serializer/deserialize/deserialization_error.h++>

#include <rapidjson/document.h>

#include <cstddef>

rapidjson::Value const& operator>>(rapidjson::Value const& value, std::nullptr_t) {
  if (!value.IsNull())
    throw deserialization_error("expected json::null found %s.", value.GetType());

  return value;
}

rapidjson::Value const& operator>>(rapidjson::Value const& value, bool& x) {
  if (!value.IsBool())
    throw deserialization_error("expected json::bool found %s.", value.GetType());

  x = value.GetBool();
  return value;
}

#include <cstdint>

rapidjson::Value const& operator>>(rapidjson::Value const& value, int& x) {
  if (!value.IsInt())
    throw deserialization_error("expected json::number<i32> found %s.", value.GetType());

  x = value.GetInt();
  return value;
}

rapidjson::Value const& operator>>(rapidjson::Value const& value, std::int64_t& x) {
  if (!value.IsInt64())
    throw deserialization_error("expected json::number<i64> found %s.", value.GetType());

  x = value.GetInt64();
  return value;
}

rapidjson::Value const& operator>>(rapidjson::Value const& value, unsigned int& x) {
  if (!value.IsUint())
    throw deserialization_error("expected json::number<u32> found %s.", value.GetType());

  x = value.GetUint();
  return value;
}

rapidjson::Value const& operator>>(rapidjson::Value const& value, std::uint64_t& x) {
  if (!value.IsUint64())
    throw deserialization_error("expected json::number<u64> found %s.", value.GetType());

  x = value.GetUint64();
  return value;
}

rapidjson::Value const& operator>>(rapidjson::Value const& value, float& x) {
  if (!value.IsFloat())
    throw deserialization_error("expected json::number<f32> found %s.", value.GetType());

  x = value.GetFloat();
  return value;
}

rapidjson::Value const& operator>>(rapidjson::Value const& value, double& x) {
  if (!value.IsDouble())
    throw deserialization_error("expected json::number<f64> found %s.", value.GetType());

  x = value.GetDouble();
  return value;
}

#include <string>

rapidjson::Value const& operator>>(rapidjson::Value const& value, std::string& string) {
  if (!value.IsString())
    throw deserialization_error("expected json::string found %s.", value.GetType());

  string = value.GetString();
  return value;
}

#include <serializer/deserialize/deserializer_array_iterator.h++>

#include <array>

template<typename T, std::size_t N>
rapidjson::Value const& operator>>(rapidjson::Value const& value, std::array<T, N>& array) {
  if (!value.IsArray())
    throw deserialization_error("expected json::array found %s.", value.GetType());

  auto const& array_value = value.GetArray();
  if (array_value.Size() != N)
    throw deserialization_error("json::array of size %zu doesn't fit in std::array<T, %zu>.", array_value.Size(), N);

  for (std::size_t index = 0; index != N; ++index)
    array_value[static_cast<rapidjson::SizeType>(index)] >> array[index];

  return value;
}

#include <set>

template<typename T>
rapidjson::Value const& operator>>(rapidjson::Value const& value, std::set<T>& set) {
  if (!value.IsArray())
    throw deserialization_error("expected json::array found %s.", value.GetType());

  auto const& array = value.GetArray();
  set = {deserializer_array_iterator<T>(array.begin()),
         deserializer_array_iterator<T>(array.end())};

  return value;
}

#include <vector>

template<typename T>
rapidjson::Value const& operator>>(rapidjson::Value const& value, std::vector<T>& rhs) {
  if (!value.IsArray())
    throw deserialization_error("expected json::array found %s.", value.GetType());

  auto const& array = value.GetArray();
  rhs = {deserializer_array_iterator<T>(array.begin()),
         deserializer_array_iterator<T>(array.end())};

  return value;
}

#include <serializer/deserialize/deserializer_object_iterator.h++>

#include <map>

template<typename Key, typename T>
rapidjson::Value const& operator>>(rapidjson::Value const& value, std::map<Key, T>& rhs) {
  static_assert(std::is_integral<Key>::value or std::is_same<Key, std::string>::value, "Key isn't an integer or a std::string.");

  if (!value.IsObject())
    throw deserialization_error("expected json::object found %s.", value.GetType());

  auto const& object = value.GetObject();
  rhs = std::map<Key, T>{deserializer_object_iterator<Key, T>(object.begin()),
                         deserializer_object_iterator<Key, T>(object.end())};

  return value;
}

#include <unordered_map>

template<typename Key, typename T>
rapidjson::Value const& operator>>(rapidjson::Value const& value, std::unordered_map<Key, T>& rhs) {
  static_assert(std::is_integral<Key>::value or std::is_same<Key, std::string>::value, "Key isn't an integer or a std::string.");

  if (!value.IsObject())
    throw deserialization_error("expected json::object found %s.", value.GetType());

  auto const& object = value.GetObject();
  rhs = std::unordered_map<Key, T>{deserializer_object_iterator<Key, T>(object.begin()),
                                   deserializer_object_iterator<Key, T>(object.end())};

  return value;
}

/* User-friendly definitions. */

#include <cassert>

template<typename T>
T from_json(char const* serialized) {
  assert("serialized is null" and serialized);
  if (!serialized)
    throw std::runtime_error("can't deserialize: \"serialized\" is null.");

  rapidjson::Document document;
  if (document.Parse(serialized).HasParseError())
    throw deserialization_error("%s at offset %zu.", document.GetParseError(), document.GetErrorOffset());

  T result;
  document >> result;

  return result;
}

template<typename T>
T from_json(std::string const& serialized) { return from_json<T>(serialized.c_str()); }

#include <serializer/deserialize/deserializer_wrapper.h++>
#include <rapidjson/istreamwrapper.h>
#include <istream>

template<typename Char, typename Traits, typename T>
std::basic_istream<Char, Traits>& operator>>(std::basic_istream<Char, Traits>& stream, deserializer_wrapper<T>&& deserializer_wrapper) {
  rapidjson::BasicIStreamWrapper<std::basic_istream<Char, Traits>> wrapper(stream);
  rapidjson::Document document;
  if (document.ParseStream(wrapper).HasParseError())
    throw deserialization_error("%s at offset %zu.", document.GetParseError(), document.GetErrorOffset());

  document >> deserializer_wrapper.wrapped;

  return stream;
}

#endif
