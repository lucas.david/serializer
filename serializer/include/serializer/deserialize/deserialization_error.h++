#ifndef SERIALIZER_DESERIALIZE_DESERIALIZATION_ERROR
#define SERIALIZER_DESERIALIZE_DESERIALIZATION_ERROR

#include <serializer/format.h++>

#include <stdexcept>

struct deserialization_error : public std::runtime_error {
public:
  template<typename... Args>
  deserialization_error(char const* fmt, Args&&... args) : std::runtime_error(format(fmt, format(args)...)) {}
};

#endif
