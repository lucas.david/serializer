
#ifndef SERIALIZER_DESERIALIZE_DESERIALIZER_OBJECT_ITERATOR
#define SERIALIZER_DESERIALIZE_DESERIALIZER_OBJECT_ITERATOR

#include <rapidjson/document.h>

#include <iterator>
#include <string>
#include <type_traits>
#include <utility>

struct deserializer_object_iterator_base {
public:
  using difference_type = std::ptrdiff_t;

  deserializer_object_iterator_base(rapidjson::Value::ConstObject::ConstMemberIterator iterator) noexcept : iterator_(iterator) {}

  deserializer_object_iterator_base& operator++() noexcept {
    ++iterator_;
    return *this;
  }

  deserializer_object_iterator_base operator++(int) noexcept {
    deserializer_object_iterator_base result = *this;
    iterator_++;
    return result;
  }

  deserializer_object_iterator_base& operator--() noexcept {
    --iterator_;
    return *this;
  }

  deserializer_object_iterator_base operator--(int) noexcept {
    deserializer_object_iterator_base result = *this;
    iterator_--;
    return result;
  }

  deserializer_object_iterator_base& operator+=(difference_type n) noexcept {
    iterator_ += n;
    return *this;
  }

  deserializer_object_iterator_base& operator-=(difference_type n) noexcept {
    iterator_ -= n;
    return *this;
  }

  deserializer_object_iterator_base operator+(difference_type n) const noexcept {
    deserializer_object_iterator_base result = *this;
    result.iterator_ = iterator_ + n;
    return result;
  }

  deserializer_object_iterator_base operator-(difference_type n) const noexcept {
    deserializer_object_iterator_base result = *this;
    result.iterator_ = iterator_ - n;
    return result;
  }

  difference_type operator-(deserializer_object_iterator_base rhs) const noexcept { return iterator_ - rhs.iterator_; }

  bool operator==(deserializer_object_iterator_base rhs) const noexcept { return iterator_ == rhs.iterator_; }
  bool operator!=(deserializer_object_iterator_base rhs) const noexcept { return iterator_ != rhs.iterator_; }
  bool operator<=(deserializer_object_iterator_base rhs) const noexcept { return iterator_ <= rhs.iterator_; }
  bool operator>=(deserializer_object_iterator_base rhs) const noexcept { return iterator_ >= rhs.iterator_; }
  bool operator<(deserializer_object_iterator_base rhs) const noexcept { return iterator_ < rhs.iterator_; }
  bool operator>(deserializer_object_iterator_base rhs) const noexcept { return iterator_ > rhs.iterator_; }

  auto const& operator*() const { return iterator_; }

  private:
    rapidjson::Value::ConstObject::ConstMemberIterator iterator_;
};

template<typename Key, typename T>
struct deserializer_object_iterator : public std::iterator<std::random_access_iterator_tag, std::pair<Key const, T>>,
                                      public deserializer_object_iterator_base {
public:
  static_assert(std::is_default_constructible<Key>::value, "Key is not default constructible.");
  static_assert(std::is_default_constructible<T>::value, "T is not default constructible.");
  static_assert(std::is_integral<Key>::value, "Key isn't an integer.");

  using iterator_category = typename std::iterator<std::bidirectional_iterator_tag, std::pair<Key const, T>>::iterator_category;
  using value_type        = typename std::iterator<std::bidirectional_iterator_tag, std::pair<Key const, T>>::value_type;
  using difference_type   = typename std::iterator<std::bidirectional_iterator_tag, std::pair<Key const, T>>::difference_type;
  using pointer           = typename std::iterator<std::bidirectional_iterator_tag, std::pair<Key const, T>>::pointer;
  using reference         = typename std::iterator<std::bidirectional_iterator_tag, std::pair<Key const, T>>::reference;

  using deserializer_object_iterator_base::deserializer_object_iterator_base;
  using deserializer_object_iterator_base::operator++;
  using deserializer_object_iterator_base::operator--;
  using deserializer_object_iterator_base::operator+=;
  using deserializer_object_iterator_base::operator-=;
  using deserializer_object_iterator_base::operator+;
  using deserializer_object_iterator_base::operator-;
  using deserializer_object_iterator_base::operator==;
  using deserializer_object_iterator_base::operator!=;
  using deserializer_object_iterator_base::operator<=;
  using deserializer_object_iterator_base::operator>=;
  using deserializer_object_iterator_base::operator<;
  using deserializer_object_iterator_base::operator>;

  value_type operator*() const {
    std::pair<Key, T> result;

    auto const& item = deserializer_object_iterator_base::operator*();
    if /*constexpr*/ (std::is_signed<Key>::value)
      result.first = static_cast<Key>(std::stoll(item->name.GetString()));
    else if /*constexpr*/ (std::is_unsigned<Key>::value)
      result.first = static_cast<Key>(std::stoull(item->value.GetString()));
    item->value >> result.second;

    return result;
  }
};

template<typename T>
struct deserializer_object_iterator<std::string, T> : public std::iterator<std::bidirectional_iterator_tag, std::pair<std::string const, T>>,
                                                      public deserializer_object_iterator_base {
public:
  static_assert(std::is_default_constructible<T>::value, "T is not default constructible.");

  using iterator_category = typename std::iterator<std::bidirectional_iterator_tag, std::pair<std::string const, T>>::iterator_category;
  using value_type        = typename std::iterator<std::bidirectional_iterator_tag, std::pair<std::string const, T>>::value_type;
  using difference_type   = typename std::iterator<std::bidirectional_iterator_tag, std::pair<std::string const, T>>::difference_type;
  using pointer           = typename std::iterator<std::bidirectional_iterator_tag, std::pair<std::string const, T>>::pointer;
  using reference         = typename std::iterator<std::bidirectional_iterator_tag, std::pair<std::string const, T>>::reference;

  using deserializer_object_iterator_base::deserializer_object_iterator_base;
  using deserializer_object_iterator_base::operator++;
  using deserializer_object_iterator_base::operator--;
  using deserializer_object_iterator_base::operator+=;
  using deserializer_object_iterator_base::operator-=;
  using deserializer_object_iterator_base::operator+;
  using deserializer_object_iterator_base::operator-;
  using deserializer_object_iterator_base::operator==;
  using deserializer_object_iterator_base::operator!=;
  using deserializer_object_iterator_base::operator<=;
  using deserializer_object_iterator_base::operator>=;
  using deserializer_object_iterator_base::operator<;
  using deserializer_object_iterator_base::operator>;

  value_type operator*() const {
    auto const& item = deserializer_object_iterator_base::operator*();

    T value;
    item->value >> value;

    return {item->name.GetString(), std::move(value)};
  }
};

#endif
