
#ifndef SERIALIZER_DESERIALIZE_DESERIALIZER_WRAPPER
#define SERIALIZER_DESERIALIZE_DESERIALIZER_WRAPPER

template<typename T>
struct deserializer_wrapper {
public:
  deserializer_wrapper(T& value) : wrapped(value) {}
  deserializer_wrapper& operator=(T const& value) { wrapped = value; return *this; }
  deserializer_wrapper& operator=(T&& value) { wrapped = std::move(value); return *this; }

  T& wrapped;
};

template<typename T>
deserializer_wrapper<T> make_deserializer_wrapper(T& value) { return deserializer_wrapper<T>(value); }

#endif
