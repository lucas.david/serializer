#ifndef SERIALIZER_FORMAT
#define SERIALIZER_FORMAT

#include <rapidjson/document.h>

#include <cstring>
#include <string>

char const* format(rapidjson::ParseErrorCode);
char const* format(rapidjson::Type);

template<typename T>
T format(T&& value) { return std::forward<T>(value); }

template<typename... Args>
std::string format(char const* fmt, Args&&... args) {
  auto const length = static_cast<std::size_t>(std::snprintf(nullptr, 0, fmt, args...));
  auto buffer = std::unique_ptr<char[]>(new char[length]);
  std::sprintf(buffer.get(), fmt, args...);
  return std::string().assign(buffer.release(), length);
}

#endif