#ifndef SERIALIZER_DESERIALIZE_SERIALIZATION_ERROR
#define SERIALIZER_DESERIALIZE_SERIALIZATION_ERROR

#include <serializer/format.h++>

#include <stdexcept>

struct serialization_error : public std::runtime_error {
public:
  template<typename... Args>
  serialization_error(char const* fmt, Args&&... args) : std::runtime_error(format(fmt, format(args)...)) {}
};

#endif
