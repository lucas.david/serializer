#ifndef SERIALIZER_SERIALIZE_COMMON
#define SERIALIZER_SERIALIZE_COMMON

#include <serializer/serialize/serialization_error.h++>

#include <rapidjson/writer.h>

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::nullptr_t) {
  if (!writer.Null())
    throw serialization_error("failed to write json::null.");

  return writer;
}

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, bool boolean) {
  if (!writer.Bool(boolean))
    throw serialization_error("failed to write json::bool.");

  return writer;
}

#include <cstdint>

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, int x) {
  if (!writer.Int(x))
    throw serialization_error("failed to write json::number<i32>.");

  return writer;
}

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::int64_t x) {
  if (!writer.Int64(x))
    throw serialization_error("failed to write json::number<i64>.");

  return writer;
}

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, unsigned int x) {
  if (!writer.Uint(x))
    throw serialization_error("failed to write json::number<u32>.");

  return writer;
}

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::uint64_t x) {
  if (!writer.Uint64(x))
    throw serialization_error("failed to write json::number<u64>.");

  return writer;
}

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, double x) {
  if (!writer.Double(x))
    throw serialization_error("failed to write json::number<f64>.");

  return writer;
}

#include <cstring>

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, char const* str) {
  assert("serialized is null" and str);
  if (!str)
    throw std::runtime_error("can't write json::string: str is null.");

  if (!writer.String(str, static_cast<rapidjson::SizeType>(std::strlen(str))))
    throw serialization_error("failed to write json::string.");

  return writer;
}

#include <string>

template<typename Buffer>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::string const& string) {
  if (!writer.String(string.data(), static_cast<rapidjson::SizeType>(string.length())))
    throw serialization_error("failed to write json::string.");

  return writer;
}

#include <array>

template<typename Buffer, typename T, std::size_t N>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::array<T, N> const& array) {
  if (!writer.StartArray())
    throw serialization_error("failed to start array.");
  for (auto const& item: array)
    writer << item;
  if (!writer.EndArray())
    throw serialization_error("failed to start array.");

  return writer;
}

#include <set>

template<typename Buffer, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::set<T> const& set) {
  if (!writer.StartArray())
    throw serialization_error("failed to start array.");
  for (auto const& item: set)
    writer << item;
  if (!writer.EndArray())
    throw serialization_error("failed to start array.");

  return writer;
}

#include <vector>

template<typename Buffer, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::vector<T> const& vector) {
  if (!writer.StartArray())
    throw serialization_error("failed to start array.");
  for (auto const& item: vector)
    writer << item;
  if (!writer.EndArray())
    throw serialization_error("failed to start array.");

  return writer;
}

#include <map>

template<typename Buffer, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::map<std::string, T> const& map) {
  if (!writer.StartObject())
    throw serialization_error("failed to start object.");
  for (auto const& item: map)
    writer << item.first << item.second;
  if (!writer.EndObject())
    throw serialization_error("failed to end object.");

  return writer;
}

template<typename Buffer, typename Key, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::map<Key, T> const& map) {
  static_assert(std::is_integral<Key>::value, "Key is not an integer.");

  if (!writer.StartObject())
    throw serialization_error("failed to start object.");
  for (auto const& item: map)
    writer << std::to_string(item.first) << item.second;
  if (!writer.EndObject())
    throw serialization_error("failed to end object.");

  return writer;
}

#include <unordered_map>

template<typename Buffer, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::unordered_map<std::string, T> const& unordered_map) {
  if (!writer.StartObject())
    throw serialization_error("failed to start object.");
  for (auto const& item: unordered_map)
    writer << item.first << item.second;
  if (!writer.EndObject())
    throw serialization_error("failed to end object.");

  return writer;
}

template<typename Buffer, typename Key, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::unordered_map<Key, T> const& unordered_map) {
  static_assert(std::is_integral<Key>::value, "Key is not an integer.");

  if (!writer.StartObject())
    throw serialization_error("failed to start object.");
  for (auto const& item: unordered_map)
    writer << std::to_string(item.first) << item.second;
  if (!writer.EndObject())
    throw serialization_error("failed to end object.");

  return writer;
}

#include <initializer_list>

template<typename Buffer, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::initializer_list<std::pair<std::string, T>> initializer_list) {
  if (!writer.StartObject())
    throw serialization_error("failed to start object.");
  for (auto const& item: initializer_list)
    writer << item.first << item.second;
  if (!writer.EndObject())
    throw serialization_error("failed to start object.");

  return writer;
}

template<typename Buffer, typename Key, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::initializer_list<std::pair<Key, T>> initializer_list) {
  static_assert(std::is_integral<Key>::value, "Key is not an integer.");

  if (!writer.StartObject())
    throw serialization_error("failed to start object.");
  for (auto const& item: initializer_list)
    writer << std::to_string(item.first) << item.second;
  if (!writer.EndObject())
    throw serialization_error("failed to start object.");

  return writer;
}

template<typename Buffer, typename T>
rapidjson::Writer<Buffer>& operator<<(rapidjson::Writer<Buffer>& writer, std::initializer_list<T> initializer_list) {
  if (!writer.StartArray())
    throw serialization_error("failed to start array.");
  for (auto const& item: initializer_list)
    writer << item;
  if (!writer.EndArray())
    throw serialization_error("failed to start array.");

  return writer;
}

#include <rapidjson/stringbuffer.h>

template<typename T>
std::string to_json(std::initializer_list<T> initializer_list) {
  rapidjson::StringBuffer buffer;
  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

  writer << initializer_list;

  return buffer.GetString();
}

template<typename T>
std::string to_json(T const& value)
{
  rapidjson::StringBuffer buffer;
  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

  writer << value;

  return buffer.GetString();
}

#include <rapidjson/ostreamwrapper.h>
#include <ostream>

template<typename Char, typename Traits, typename T>
std::basic_ostream<Char, Traits>& operator<<(std::basic_ostream<Char, Traits>& stream, T const& value) {
  rapidjson::BasicOStreamWrapper<std::basic_ostream<Char, Traits>> wrapper(stream);
  rapidjson::Writer<rapidjson::BasicOStreamWrapper<std::basic_ostream<Char, Traits>>> writer(wrapper);

  writer << value;

  return stream;
}

#endif
